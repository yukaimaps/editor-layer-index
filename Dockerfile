FROM scratch
COPY ./imagery.json /imagery/
COPY ./imagery.xml /imagery/
COPY ./imagery.geojson /imagery/
